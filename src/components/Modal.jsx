import './Modal.scss'

function Modal({ children }) {
  return <div className="modal">{children}</div>
}

export default Modal
