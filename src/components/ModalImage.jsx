import Modal from './Modal'
import Header from './ModalComponents/ModalHeader'
import Footer from './ModalComponents/ModalFooter'
import Body from './ModalComponents/ModalBody'
import Close from './ModalComponents/ModalClose'
import Wrapper from './ModalComponents/ModalWrapper'
import Button from './Button'
import Image from '../img/images.jfif'

function ModalImage({ modal, setModal }) {
  return modal ? (
    <Wrapper
      onClick={(event) => {
        if (event.target.classList.contains('modal__wrapper')) {
          setModal(!modal)
        }
      }}
    >
      <Modal>
        <Header>
          <Close
            onClick={() => {
              setModal(!modal)
            }}
          />
        </Header>

        <Body>
          <img src={Image} className="modal__body-img" alt="" />
          <h2 className="modal__body-title">Product Delete!</h2>
          <p className="modal__body-text">
            By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted.
          </p>
        </Body>
        <Footer>
          <Button className="modal__footer-btn footer-btn2 btn">
            NO, CANCEL
          </Button>
          <Button className="modal__footer-btn footer-btn1 btn">
            YES, DELETE
          </Button>
        </Footer>
      </Modal>
    </Wrapper>
  ) : null
}

export default ModalImage
