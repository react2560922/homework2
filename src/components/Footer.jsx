import './Footer.scss'

function Footer() {
  return (
    <footer className="footer-conteiner">
      <div className="footer-content conteiner">
        <p className="footer-content__text">&copy;2023</p>
        <p className="footer-content__text">All rights reserved</p>
      </div>
    </footer>
  )
}
export default Footer
