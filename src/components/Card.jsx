import './Card.scss'
import Button from './Button'
import { useState, useEffect } from 'react'
import { addToCart, addToFavorites } from './utils/localStorageEvents'
import ModalTextAdd from './ModalTextAdd'

function Card({
  setCartCounter,
  cartCounter,
  favoritesCounter,
  setFavoritesCounter,
  isActive,
  setIsActive,
  setIsActiveFav,
  isActiveFav,
}) {
  const [products, setProducts] = useState([])
  const [modalProduct, setModalProduct] = useState(null)
  useEffect(() => {
    fetch('./products.json')
      .then((response) => response.json())
      .then((data) => setProducts(data))
      .catch((error) => console.error('Error fetching products:', error))
  }, [])

  const handleAddToCart = (productItem) => {
    addToCart(
      setModalProduct,
      setIsActive,
      productItem,
      cartCounter,
      setCartCounter
    )
    setModalProduct(productItem)
    setTimeout(() => setModalProduct(null), 1000)
  }

  return (
    <>
      <div className="bestsellers-content conteiner">
        <ul className="bestsellers-content__list">
          {products.map((productItem) => (
            <li key={productItem.sku} className="bestsellers-content__item">
              {modalProduct && modalProduct.sku === productItem.sku && (
                <ModalTextAdd
                  modal={!!modalProduct}
                  setModal={() => setModalProduct(null)}
                  nameProduct={modalProduct.name}
                />
              )}
              <div className="bestsellers-content__offers">
                <svg
                  onClick={() =>
                    addToFavorites(
                      setIsActiveFav,
                      productItem,
                      favoritesCounter,
                      setFavoritesCounter
                    )
                  }
                  className={
                    isActiveFav[productItem.sku]
                      ? 'bestsellers-content__offers-img active-favorites'
                      : 'bestsellers-content__offers-img'
                  }
                  width="40"
                  height="40"
                  viewBox="0 0 34 34"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <circle cx="17.1291" cy="16.9111" r="16.1796" fill="white" />
                  <path
                    d="M16.7198 12.7121C15.345 11.1099 13.0523 10.6789 11.3298 12.146C9.60723 13.6131 9.36472 16.0661 10.7174 17.8013C11.8422 19.244 15.2459 22.2867 16.3615 23.2716C16.4863 23.3817 16.5487 23.4368 16.6215 23.4585C16.685 23.4774 16.7545 23.4774 16.8181 23.4585C16.8909 23.4368 16.9533 23.3817 17.0781 23.2716C18.1936 22.2867 21.5974 19.244 22.7221 17.8013C24.0748 16.0661 23.8619 13.5977 22.1098 12.146C20.3576 10.6943 18.0946 11.1099 16.7198 12.7121Z"
                    stroke="#3C4242"
                  />
                </svg>
              </div>

              <img
                src={productItem.imageUrl}
                alt=""
                className="bestsellers-content__link-img"
              />
              <p className="bestsellers-content__link-name">
                {productItem.name}
              </p>

              <div className="bestsellers-content__price">
                <p className="price-bestsellers">{productItem.price} €</p>
              </div>

              <Button
                onClick={() => handleAddToCart(productItem)}
                className={
                  isActive[productItem.sku]
                    ? 'bestsellers-content__add-to-basket active-cart'
                    : 'bestsellers-content__add-to-basket'
                }
              >
                {isActive[productItem.sku]
                  ? 'Товар додано до кошика'
                  : 'Додати товар до кошика'}
              </Button>
            </li>
          ))}
        </ul>
      </div>
    </>
  )
}

export default Card
