import './Header.scss'
import Button from './Button'
import logo from '../img/logo.png'

function Header({ cartCounter, favoritesCounter }) {
  return (
    <div className="header-top">
      <div className="header-top__content conteiner">
        <a href="#" className="header-top__logo">
          <img src={logo} alt="logo" className="header-top__logo-img" />
        </a>

        <div className="header-top__basket">
          <Button className="header-top__basket-favorites" type="button">
            {favoritesCounter.length}
          </Button>
          <Button className="header-top__basket-img" type="button">
            {cartCounter.length}
          </Button>
        </div>
      </div>
    </div>
  )
}

export default Header
