import favorites from '../img/favorites.svg'
import Button from './Button'

function ProductCard({ imageUrl, name, price, sku, index }) {
  function addToCard() {
    const cart = JSON.parse(localStorage.getItem('cart')) || []

    if (!cart.includes(sku)) {
      cart.push(sku)
      localStorage.setItem('cart', JSON.stringify(cart))
    }
  }

  return (
    <>
      <li key={index} className="bestsellers-content__item">
        <div className="bestsellers-content__offers">
          <img
            className="bestsellers-content__offers-img"
            src={favorites}
            alt="favorites"
          />
        </div>

        <img src={imageUrl} alt="" className="bestsellers-content__link-img" />
        <p className="bestsellers-content__link-name">{name}</p>

        <div className="bestsellers-content__price">
          <p className="price-bestsellers-new">{price} €</p>
        </div>
        <Button
          onClick={addToCard}
          className="bestsellers-content__add-to-basket"
          type="button"
        >
          Add to cart
        </Button>
      </li>
    </>
  )
}

export default ProductCard
