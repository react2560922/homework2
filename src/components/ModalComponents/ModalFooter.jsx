import './ModalFooter.scss'

function ModalClose({ children }) {
  return <div className="modal__footer">{children}</div>
}
export default ModalClose
