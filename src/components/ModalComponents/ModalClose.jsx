import './ModalClose.scss'

function ModalClose({ onClick }) {
  return (
    <div className="modal__close">
      <svg
        onClick={onClick}
        width="22"
        height="22"
        viewBox="0 0 22 22"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path d="M18 4L4 18M18 18L4 4.00001" stroke="#3C4242" />
      </svg>
    </div>
  )
}
export default ModalClose
