import { useState, useEffect } from 'react'
import Button from './components/Button'
import Header from './components/Header'
import Card from './components/Card'
import Footer from './components/Footer'

import './style.scss'

function App() {
  const cart = JSON.parse(localStorage.getItem('card')) || []
  const favorites = JSON.parse(localStorage.getItem('favorites')) || []

  const [cartCounter, setCartCounter] = useState(cart)
  const [favoritesCounter, setFavoritesCounter] = useState(favorites)
  const [isActive, setIsActive] = useState(false)
  const [isActiveFav, setIsActiveFav] = useState(false)

  useEffect(() => {
    cart.map((elem) => {
      setIsActive((prevState) => ({ ...prevState, [elem.sku]: true }))
    })
    favorites.map((elem) => {
      setIsActiveFav((prevState) => ({ ...prevState, [elem.sku]: true }))
    })
  }, [])

  return (
    <>
      <Header cartCounter={cartCounter} favoritesCounter={favoritesCounter} />

      <Card
        setCartCounter={setCartCounter}
        cartCounter={cartCounter}
        favoritesCounter={favoritesCounter}
        setFavoritesCounter={setFavoritesCounter}
        isActive={isActive}
        setIsActive={setIsActive}
        setIsActiveFav={setIsActiveFav}
        isActiveFav={isActiveFav}
      />

      <Footer />
    </>
  )
}

export default App
